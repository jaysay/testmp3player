@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1>Your Music</h1>
                    <div class="panel-body">
                         <form action="{{ url('crud') }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <label>Song Title:</label>
                            <input class="form-control" type="text" name="title"><br>
                            <label class="btn btn-primary btn-file">
                            Browse <input type="file" name="file" style="display: none;">
                            </label>
                            <button value="submit" type="submit" class="btn btn-success">Add Song</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
