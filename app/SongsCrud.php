<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SongsCrud extends Model
{
   protected $table = 'songs';
   protected $primaryKey = 'id';
   protected $fillable = [
   		'title',
   		'file',
   ];
}
